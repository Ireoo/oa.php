/**
 * Created with JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-8-30
 * Time: 下午3:00
 * To change this template use File | Settings | File Templates.
 */
var photobox = function(divID, result) {
    var cedit = $('<button onclick="return false;">上传照片</button>').css({fontSize: '12px', fontWeight: 'normal', padding: '5px', borderRadius: '3px', marginLeft: '10px'});
    $(divID).after(cedit).remove();
    cedit.click(function() {
        $('<div id="showedit"><div id="avatar"></div></div>').appendTo('body').css({position: "fixed", background: "#CCC", padding: '3px'});
        xiuxiu.setLaunchVars ("nav", "edit");
        xiuxiu.embedSWF("avatar", 1, 1000, 400);
        xiuxiu.onInit = function ()
        {
            xiuxiu.loadPhoto("");
        };
        xiuxiu.setUploadURL("http://oa.ireoo.com/app/xiuxiu/photo.php");
        var divID = $('#showedit');
        var winHeight = $(window).height();
        var winWidth = $(window).width();
        var divHeight = divID.height();
        var divWidth = divID.width();
        var top = (winHeight - divHeight) / 2;
        var left = (winWidth - divWidth) / 2;
        divID.css({ top: top + "px", left: left + "px"});
    });
    xiuxiu.onClose = function()
    {
        $('#showedit').remove();
    };
    xiuxiu.onBeforeUpload = function(data, id) {
	    //alert(data);
    };
    xiuxiu.onUploadResponse= function(data,id) {
        alert('照片已上传！');
        //location.reload();
        //cedit.parent('li').find('input').val(data);
        $(result).val(data);
        $('#showedit').remove();
    };
    xiuxiu.onDebug = function (data)
    {
        alert(data);
    };
}
