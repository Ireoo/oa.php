<?php
$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
        if($mysql->insert('user', $_POST)) {
            echo '<script type="text/javascript">alert("会员信息已经保存！");</script>';
        }else{
            echo mysql_error();
            print_r($_POST);
        }
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /?s=user&i=add");
    }
}
?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    div.mian ol li a{font-size: 14px; cursor: pointer;}
    div.mian ol li a:hover{text-decoration: underline;}

    div.mian ol li label{display: inline-block; width: 100px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    div.mian ol li input{padding: 5px; font-size: 12px; width: 272px;}
    div.mian ol li input.check, div.mian ol li label.auto{width: auto;}
    div.mian ol li select.max{width: 212px;}
    div.mian ol li select{padding: 5px;}

    div.mian ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    div.mian ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    div.mian ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    div.mian ol li ul li.hover{background: #4898F8; color: #FFF;}
    div.mian ol li ul li.hover ul{display: inline-block;}
    div.mian ol li ul li ul li{display: block; font-size: 12px;}
    div.mian ol li ul li ul li a{font-size: 12px;}
    div.mian ol li ul li ul li a:hover, div.mian ol li ul li ul li:hover a, div.mian ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    div.mian ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script type="text/javascript" src="/include/js/photobox.js"></script>
<script type="text/javascript" src="http://open.web.meitu.com/sources/xiuxiu.js"></script>
<script type="text/javascript">
$(function() {
	photobox('#upload', '#cardPhoto');
	photobox('#upload_d', '#cardPhoto_d');
});
</script>

<ol class="account">

    <form action="?s=user&i=add&token=<?php echo md5(rand(0, 100000000)); ?>" method="post">
        <h1>基本资料</h1>
        <span class="h2"></span>
        <li>
            <label>姓名：</label>
            <input type="text" name="s_name" value="" />
        </li>
        <li>
            <label>性别：</label>
            <select name="s_sex">
            	<option value="男">男</option>
            	<option value="女">女</option>
            </select>
        </li>
        <li>
            <label>身份证：</label>
            <input type="text" name="s_card" value="" />
        </li>
        <li>
            <label>身份证照片：</label>
            <input type="text" readonly id="cardPhoto" name="s_cardPhoto" value="" /><div id="upload" style="display: inline-block; padding-left: 20px;"></div>
        </li>
        <li>
            <label>联系电话：</label>
            <input type="text" name="s_phone" value="" />
        </li>

        <li>
            <label>现居地址：</label>
            <input type="text" name="s_address" value="" />
        </li>

        <li class="textarea">
            <label>备注：</label>
            <textarea name="s_beizhu"></textarea>
        </li>
        
        
        <h1>担保人</h1>
        <span class="h2"></span>
        <li>
            <label>担保人姓名：</label>
            <input type="text" name="s_name_d" value="" />
        </li>
        <li>
            <label>担保人性别：</label>
            <select name="s_sex">
            	<option value="男">男</option>
            	<option value="女">女</option>
            </select>
        </li>
        <li>
            <label>担保人电话：</label>
            <input type="text" name="s_phone_d" value="" />
        </li>
        
        <li>
            <label>担保人身份证：</label>
            <input type="text" name="s_card_d" value="" />
        </li>
        <li>
            <label>担保人身份证照片：</label>
            <input type="text" readonly id="cardPhoto_d" name="s_cardphoto_d" value="" /><div id="upload_d" style="display: inline-block; padding-left: 20px;"></div>
        </li>
        
        <li class="bu">
            <button>保存</button>
            <span class="result"></span>
        </li>
    </form>
</ol>