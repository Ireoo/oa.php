<?php
/**
 * Created by PhpStorm.
 * User: ireoo
 * Date: 14-1-22
 * Time: 下午8:12
 */

?>

<?php



$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
		$sql['condition'] = "{$_POST['select']} = '{$_POST['value']}'";
        //print_r($_POST);
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /?s=user&i=index");
    }
}
//print_r($sql);


?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol table{border-collapse: collapse; width: 100%; margin-bottom: 10px;}
    div.mian ol table tr{margin-bottom: 10px;}
    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}
    
    div.foot{height: 32px; text-align: center;}
    div.foot a{display: inline-block; padding: 0 10px; line-height: 30px; color: #FFF; border: 1px #333 solid;}
    div.foot a.on{background: #FFF; color: #333;}
</style>

<script src="/app/layer/layer.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app/layer/skin/layer.ext.css">
<script type="text/javascript">
	$(
		function() {
			$('a.btn').click(
				function() {
					id = $(this).attr('id');
					$.layer({
					    type : 2,
					    title : '详细信息',
					    iframe : {src : '/xiangxi/user.php?id=' + id},
					    area : ['750px' , '466px'],
					    offset : ['100px','100px']
					});
				}
			);
		}
	);
</script>

<ol>
    <h1>客户管理</h1>
    
    <form action="?s=user&i=index&token=<?php echo md5(rand(0, 100000000)); ?>" method="post" style="margin-bottom: 10px;">
	    <select name="select">
	    	<option value="s_name">姓名</option>
	    	<option value="s_card">身份证</option>
	    	<option value="s_phone">电话</option>
	    </select>
	    <input type="text" name="value" style="padding: 6px;" value="" />
	    <button>搜索</button>
    </form>

    <table>
        <thead>
        <tr>
            <td>姓名</td>
            <td>身份证</td>
            <td>联系电话</td>
            <td>地址</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php
        
        $now = 0;
        $show = 20;
        
        if(isset($_GET['y'])) {
	        $now = $_GET['y'];
        }
        
        $start = $now*$show;
        
        $sql = array(
		    'table' => 'user'
		);
		
		$zong = $mysql->select($sql);
        
        $list = $sql;
        $list['limit'] = "LIMIT $start, $show";

		$re = $mysql->select($list);
        //print_r($s);
        foreach($re as $key => $value) {
            $v = $value['user'];
            ?>
            <tr>
                <td><?php echo $v['s_name']; ?></td>
	            <td><?php echo $v['s_card']; ?></td>
	            <td><?php echo $v['s_phone']; ?></td>
	            <td><?php echo $v['s_address']; ?></td>
	            <td><a class="btn" id="<?php echo $v['id']; ?>">详细</a></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    
    <div class="foot">
    	<a href="?s=user">首页</a>
    	<?php for($i=0; $i<ceil(count($zong)/$show); $i++) { ?>
    	<a<?php if($now == $i) {echo ' class="on"';} ?> href="?s=user&y=<?php echo $i; ?>"><?php echo $i+1; ?></a>
    	<?php } ?>
    	<a href="?s=user&y=<?php echo floor(count($zong)/$show); ?>">尾页</a>
    </div>

</ol>