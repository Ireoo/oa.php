<?php
/**
 * Created by PhpStorm.
 * User: ireoo
 * Date: 14-1-22
 * Time: 下午8:12
 */

?>

<?php

if(isset($_GET['id'])){
	
	if($_GET['quxiao'] == 'yes') {
		$r1 = $mysql->update('`inOut`', array('s_hei'=>0), "id = {$_GET['id']}");
	}else{
		$r1 = $mysql->update('`inOut`', array('s_hei'=>1), "id = {$_GET['id']}");
	}
	
	
};

?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol table{border-collapse: collapse; width: 100%; margin-bottom: 10px;}
    div.mian ol table tr{margin-bottom: 10px;}
    
    div.mian ol table tr.red{background: red;}
    div.mian ol table tr.red td{color: #FFF;}
    
    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}
    
    div.foot{height: 32px; text-align: center;}
    div.foot a{display: inline-block; padding: 0 10px; line-height: 30px; color: #FFF; border: 1px #333 solid;}
    div.foot a.on{background: #FFF; color: #333;}
</style>

<script src="/app/layer/layer.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app/layer/skin/layer.ext.css">
<script type="text/javascript">
	$(
		function() {
			$('a.btn').click(
				function() {
					id = $(this).attr('id');
					$.layer({
					    type : 2,
					    title : '详细信息',
					    iframe : {src : '/xiangxi/carOut.php?id=' + id},
					    area : ['750px' , '466px'],
					    offset : ['100px','100px']
					});
				}
			);
			
			$('a.weizhang').click(
				function() {
					id = $(this).attr('id');
					$.layer({
					    type : 2,
					    title : '违章信息查询',
					    iframe : {src : 'http://www.hajxj.gov.cn/jtwfcx/clwfsearch.asp'},
					    area : ['850px' , '466px'],
					    offset : ['100px','100px']
					});
				}
			);
		}
	);
</script>

<ol>
    <h1>已返还车辆管理</h1>

    <table>
        <thead>
        <tr>
            <td>车牌</td>
            <td>用户</td>
            <td>出租时间</td>
            <td>还车时间</td>
            <td>登记时间</td>
            <td>开始里程</td>
            <td>结束里程</td>
            <td>费用</td>
            <td>实收</td>
            <td>收款方式</td>
            <td>经办人</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php
        
        $now = 0;
        $show = 20;
        
        if(isset($_GET['y'])) {
	        $now = $_GET['y'];
        }
        
        $start = $now*$show;
        
        $sql = array(
            'table' => '`inOut`',
            'condition' => 'fanhuan = 1'
        );
        $zong = $mysql->select($sql);
        
        $list = $sql;
        $list['limit'] = "LIMIT $start, $show";

		$re = $mysql->select($list);
        //print_r($s);
        foreach($re as $key => $value) {
            $v = $value['inOut'];
            ?>
            <tr class="<?php if($v['s_hei'] == 1){echo 'red';} ?>">
                
                <?php
	                
	                $id = $v['s_card'];
	                $s = array(
	                	'table' => 'car',
	                	'condition' => 'id = ' . $id
	                );
	                $r = $mysql->row($s);
	                //print_r($s);
	                
                ?>
                
                <td><?php echo $r['s_card']; ?></td>
                
                <?php
	                
	                $id = $v['s_user'];
	                $s = array(
	                	'table' => 'user',
	                	'condition' => 'id = ' . $id
	                );
	                $r = $mysql->row($s);
	                //print_r($s);
	                
                ?>
                
                <td><?php echo $r['s_name']; ?></td>
                <td><?php echo date('Y-m-d', $v['s_start']); ?></td>
                <td><?php echo date('Y-m-d', $v['s_over']); ?></td>
                <td><?php echo date('Y-m-d', $v['timer']); ?></td>
                <td><?php echo $v['s_licheng']; ?></td>
                <td><?php echo $v['s_licheng_over']; ?></td>
                <td><?php echo $v['money']; ?></td>
                <td><?php echo $v['s_shishou']; ?></td>
                <td><?php echo $v['s_fangshi']; ?></td>
                <td><?php echo $v['s_jinbanren']; ?></td>
	            <td>
	            	<a class="btn" id="<?php echo $v['id']; ?>">详细</a>
	            	<?php if($v['s_hei']==0) { ?><a href="?s=carIn&i=yes&id=<?php echo $v['id']; ?>">加黑</a><?php }else{?><a href="?s=carIn&i=yes&id=<?php echo $v['id']; ?>&quxiao=yes">取消</a><?php } ?>
	            	<a class="weizhang" id="<?php echo $v['id']; ?>">违章</a>
	            </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    
    <div class="foot">
    	<a href="?s=carIn&i=yes">首页</a>
    	<?php for($i=0; $i<ceil(count($zong)/$show); $i++) { ?>
    	<a<?php if($now == $i) {echo ' class="on"';} ?> href="?s=carIn&i=yes&y=<?php echo $i; ?>"><?php echo $i+1; ?></a>
    	<?php } ?>
    	<a href="?s=carIn&i=yes&y=<?php echo floor(count($zong)/$show); ?>">尾页</a>
    </div>

</ol>