<?php
/**
 * Created by PhpStorm.
 * User: ireoo
 * Date: 14-1-22
 * Time: 下午8:12
 */

?>

<?php

if(isset($_GET['id'])){

    $r1 = $mysql->update('`inOut`', array('s_hei'=>1), "id = {$_GET['id']}");

};

?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px; margin: 0;}

    div.mian ol table{border-collapse: collapse; width: 100%;}
    div.mian ol table tr{margin-bottom: 10px;}

    div.mian ol table tr.red{background: red;}
    div.mian ol table tr.red td{color: #FFF;}

    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}

    div.show{font-size: 12px; margin-bottom: 30px; display: inline-block; width: 33%; color: #666;}
    div.show h1{margin-bottom: 10px;}
    div.show li{list-style: none; display: inline-block; width: 49%;}
    div.show li span{font-size: 24px;}
    div.show li span.in{color: red;}
    div.show li span.out{color: green;}

    div.showMax{font-size: 12px; margin-bottom: 30px; display: inline-block; width: 100%; color: #666;}
    div.showMax h1{margin-bottom: 10px;}
    div.showMax li{list-style: none; display: inline-block; width: 33%;}
    div.showMax li span{font-size: 24px;}
    div.showMax li span.in{color: red;}
    div.showMax li span.out{color: green;}

    li.tx{padding: 5px; font-size: 12px; color: #FFF; margin-bottom: 5px;}
    li.tx.red{background: red;}

</style>

<script src="/app/layer/layer.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app/layer/skin/layer.ext.css">
<script type="text/javascript">
	$(
		function() {
			$('button#tixing').click(
				function() {
					$.layer({
					    type : 2,
					    title : '添加提醒项目',
					    iframe : {src : '/xiangxi/tixing.php'},
					    area : ['420px' , '250px'],
					    offset : ['100px','100px']
					});
				}
			);
			
		}
	);
</script>

<ol>


    <?php
    $sql = array(
        'table' => 'money',
        'condition' => 'timer > ' . strtotime(date('Y-m'))
    );
    $re = $mysql->select($sql);

    $zongshouru = 0;
    $zongzhichu = 0;

    foreach($re as $k => $v) {
        $l = $v['money'];
        if($l['s_fangshi'] == 'in') {
            $zongshouru += $l['money'];
        }else{
            $zongzhichu += $l['money'];
        }
    }

    ?>

    <div class="show">
        <h1>公司月财务状况</h1>
        <li>总收入：<span class="in"><?php echo $zongshouru; ?></span></li>
        <li>总支出：<span class="out"><?php echo $zongzhichu; ?></span></li>
    </div>

    <?php
    $sql = array(
        'table' => 'money',
        'condition' => 'timer > ' . strtotime(date('Y-m-d'))
    );
    $re = $mysql->select($sql);

    $zongshouru = 0;
    $zongzhichu = 0;

    foreach($re as $k => $v) {
        $l = $v['money'];
        if($l['s_fangshi'] == 'in') {
            $zongshouru += $l['money'];
        }else{
            $zongzhichu += $l['money'];
        }
    }

    ?>

    <div class="show">
        <h1>今日财务状况</h1>
        <li>总收入：<span class="in"><?php echo $zongshouru; ?></span></li>
        <li>总支出：<span class="out"><?php echo $zongzhichu; ?></span></li>
    </div>



    <?php
    $sql = array(
        'table' => 'money',
        'condition' => 'timer > ' . (time() - 3600*24)
    );
    $re = $mysql->select($sql);

    $zongshouru = 0;
    $zongzhichu = 0;

    foreach($re as $k => $v) {
        $l = $v['money'];
        if($l['s_fangshi'] == 'in') {
            $zongshouru += $l['money'];
        }else{
            $zongzhichu += $l['money'];
        }
    }

    ?>

    <div class="show">
        <h1>24小时内财务状况</h1>
        <li>总收入：<span class="in"><?php echo $zongshouru; ?></span></li>
        <li>总支出：<span class="out"><?php echo $zongzhichu; ?></span></li>
    </div>

    <?php
    $sql = array(
        'table' => 'car'
    );
    $re = $mysql->select($sql);

    $cars = count($re);
    $zaizu = 0;

    foreach($re as $key => $value) {
        $v = $value['car'];
        $js = array(
            'table' => '`inOut`',
            'condition' => "s_card = " . $v['id'],
            'order' => 'id desc'
        );
        $jr = $mysql->row($js);

        if($jr['fanhuan'] == '0') {
            $zaizu ++;
        }
    }
    ?>

    <div class="showMax">
        <h1>车辆情况预览</h1>
        <li>车辆总数：<span><?php echo $cars; ?></span></li>
        <li>出租中：<span class="out"><?php echo $zaizu; ?></span></li>

    </div>

    <h1>到期提醒<button id="tixing" style="font-size: 12px; padding: 3px 5px; margin-left: 10px;">添加</button></h1>
    <h2>车辆年审，交强险，商业险及客户生日提醒</h2>


    <?php
    $sql = array(
        'table' => 'tixing',
        'condition' => 'timer > ' . time(),
        'order' => 'timer asc'
    );
    $re = $mysql->select($sql);

    foreach($re as $k => $v) {
        $l = $v['tixing'];
        $timer = date('Y-m-d', $l['timer']);
        echo "<li class='tx red'>{$l['com']}, {$timer} 到期。</li>";
    }
    
    $sql = array(
        'table' => 'car'
    );
    $re = $mysql->select($sql);
    
    //print_r(date("Y-m-d"));
    
    foreach($re as $k => $v) {
        $l = $v['car'];
        //print_r((time() - 3600*24*7 - strtotime($l['s_nianshen'])) . ' < ' . strtotime($l['s_nianshen']) . ' < ' . (time() - strtotime($l['s_nianshen'])) . '     ');
        if(strtotime($l['s_nianshen']) > (time() - 3600*24*7) and strtotime($l['s_nianshen']) < time()) {
            echo "<li class='tx red'>{$l['s_card']} 还有不到7天就要年审了！</li>";
        }
        if(strtotime($l['s_shangyexian']) > (time() - 3600*24*7) and strtotime($l['s_shangyexian']) < time()) {
            echo "<li class='tx red'>{$l['s_card']} 商业险还有不到7天就要到期了！</li>";
        }
        if(strtotime($l['s_jiaoqiangxian']) > (time() - 3600*24*7) and strtotime($l['s_jiaoqiangxian']) < time()) {
            echo "<li class='tx red'>{$l['s_card']} 交强险还有不到7天就要到期了！</li>";
        }
    }

    ?>

    <h1>还车提醒</h1>
    <h2>车辆还车提醒</h2>

    <table>
        <tr>
            <td>车牌</td>
            <td>出租时间</td>
            <td>到期时间</td>
            <td>联系人</td>
            <td>联系电话</td>
            <td>联系地址</td>
        </tr>
        
    <?php
    $sql = array(
        'table' => 'car'
    );
    $re = $mysql->select($sql);

    foreach($re as $k => $v) {
        $l = $v['car'];
        $js = array(
            'table' => '`inOut`',
            'condition' => "s_card = " . $l['id'],
            'order' => 'id desc'
        );
        $jr = $mysql->row($js);
        $us = array(
            'table' => 'user',
            'condition' => "id = " . $jr['s_user'],
            'order' => 'id desc'
        );
        $ur = $mysql->row($us);

        if($jr['fanhuan'] == '0') {
            //print_r($jr);
            //print_r($ur);
            $over = $jr['s_over']*1 + $jr['s_hour']*60*60 + $jr['s_fen']*60;
?>
		<tr>
            <td><?php echo $l['s_card'];?></td>
            <td><?php echo date('Y-m-d', $jr['s_start']) . ' ' . $jr['s_hour']. ':' . $jr['s_fen'];?></td>
            <td><?php echo date('Y-m-d', $jr['s_over']) . ' ' . $jr['s_hour']. ':' . $jr['s_fen'];?>
            <?php if($over < time()) {echo '(逾期 ' . (date('d', (time() - $over)) - 1) . ' 天)';} ?>
            </td>
            <td><?php echo $ur['s_name'];?></td>
            <td><?php echo $ur['s_phone'];?></td>
            <td><?php echo $ur['s_address'];?></td>
        </tr>
<?php
        }
    }

    ?>
        
    </table>
    <?php
//    $sql = array(
//        'table' => 'car'
//    );
//    $re = $mysql->select($sql);
//
//    foreach($re as $k => $v) {
//        $l = $v['car'];
//        $js = array(
//            'table' => '`inOut`',
//            'condition' => "s_card = " . $l['id'],
//            'order' => 'id desc'
//        );
//        $jr = $mysql->row($js);
//
//        if($jr['fanhuan'] == '0') {
//            //print_r($jr);
//            $over = $jr['s_over']*1 + $jr['s_hour']*60*60 + $jr['s_fen']*60;
//
//
//            $shijian = date('Y-m-d H:i', $over);
//
//            if($over > time()) {
//                $cha = $over - time();
//                $tian = date('d', $cha) - 1;
//                echo $cha . $tian;
//                echo "<li class='tx green'>{$l['s_card']} 还车时间 {$shijian}，还有 {$tian} 天到期。</li>";
//            }else{
//                $cha = time() - $over;
//                $tian = date('d', $cha) - 1;
//                //echo $cha . $tian;
//                echo "<li class='tx red'>{$l['s_card']} 还车时间 {$shijian}，逾期 {$tian} 天。</li>";
//            }
//        }
//    }

    ?>

</ol>