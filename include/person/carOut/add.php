<?php
$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
    	unset($_POST['s_yixingshi']);
    	$_POST['timer'] = time();
    	$_POST['s_hour'] = date('H');
    	$_POST['s_fen'] = date('i');
    	$_POST['s_type'] = 'out';
    	$r = $mysql->insert('`inOut`', $_POST);
        if($r) {
	        echo '<script type="text/javascript">alert("数据保存！");</script>';
	        $s = array(
	        	's_card' => $_POST['s_card'],
	        	's_fangshi' => 'in',
	        	's_shuoming' => '车辆出租实收款',
	        	'money' => $_POST['s_shishou'],
	        	'timer' => time()
	        );
	        $mysql->insert('money', $s);
        }else{
            echo mysql_error();
            print_r($_POST);
        }
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /?s=carOut&i=add");
    }
}
?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    div.mian ol li a{font-size: 14px; cursor: pointer;}
    div.mian ol li a:hover{text-decoration: underline;}

    div.mian ol li label{display: inline-block; width: 150px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    div.mian ol li input{padding: 5px; font-size: 12px; width: 272px;}
    div.mian ol li input.check, div.mian ol li label.auto{width: auto;}
    div.mian ol li select.max{width: 212px;}
    div.mian ol li select{padding: 5px;}
    
    div.mian ol li div.show{display: inline-block; padding: 5px;}

    div.mian ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    div.mian ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    div.mian ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    div.mian ol li ul li.hover{background: #4898F8; color: #FFF;}
    div.mian ol li ul li.hover ul{display: inline-block;}
    div.mian ol li ul li ul li{display: block; font-size: 12px;}
    div.mian ol li ul li ul li a{font-size: 12px;}
    div.mian ol li ul li ul li a:hover, div.mian ol li ul li ul li:hover a, div.mian ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    div.mian ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script language="JavaScript" type="text/javascript" src="/app/jCal/jquery.animate.clip.js"></script>
<script language="JavaScript" type="text/javascript" src="/app/jCal/jCal.min.js"></script>

<link rel="stylesheet" type="text/css" href="/app/jCal/jCal.css">

	<script>
		$(document).ready(function () {
			
				

					$('#shijian').jCal({
					day:			new Date(),
					days:			2,
					showMonths:		2,
					monthSelect:	true,
					sDate:			new Date(),
	
					callback:		function (day, days) {
							$('#s_start_v').val(day.getTime()/1000);
							$('#s_start').text(day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate());
							var lin = new Date(day.setDate( day.getDate() + days - 1 ));
							$('#s_over_v').val(lin.getTime()/1000);
							$('#s_over').text(lin.getFullYear() + '-' + (lin.getMonth() + 1) + '-' + lin.getDate());
							zujin();
							return true;
						}
					});


            $('#card').change(function() {
                var json = eval('(' + $(this).val() + ')');
                $('#pinpai').text(json.s_pinpai);
                $('#s_card').val(json.id);
                $('#xinghao').text(json.s_xinghao);
                $('#chezhu').text(json.s_chezhu);
                $('#color').text(json.s_color);
                $('#yixingshi').text(json.s_yixingshi);
                
                $('#money').text(json.s_zujin);
                $('#yajin').text(json.s_yajin);
                $('#s_yajin').val(json.s_yajin);

                $('#nianshen').text(json.s_nianshen);
                $('#jiaoqiangxian').text(json.s_jiaoqiangxian);
                $('#shangyexian').text(json.s_shangyexian);
                $('#photos').text(json.s_photo);
            });

            $('#user').change(function() {
                var json = eval('(' + $(this).val() + ')');
                $('#s_name').val(json.id);
                //alert(json.s_card);
                $('#u_card').text(json.s_card);
                $('#u_phone').text(json.s_phone);
                $('#u_address').text(json.s_address);
                $('#u_cishu').text(json.s_cishu);
            });
				
				function zujin() {
					var t = new Date();
					t.setTime($('#s_start_v').val()*1000);
					//alert($('#s_start').text());
					//alert(t.getDate());
					var c = $('#day').val();
					$('#zmoney').val(0);
					t.setDate(t.getDate() - 1);
					for (var i=0; i<c; i++) {
						t.setDate(t.getDate() + 1);
						//alert(t.getDay());
						switch(t.getDay()) {
							case 0:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1.1));
								break;
							case 1:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1));
								break;
							case 2:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1));
								break;
							case 3:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1));
								break;
							case 4:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1));
								break;
							case 5:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1));
								break;
							case 6:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $('#money').text()*1.1));
								break;
						}
						
					}
					$('#zmoney').val(parseInt($('#zmoney').val()) + parseInt($('#yajin').text()));
				}
				
		});
	</script>


<ol class="account">

    <form action="?s=carOut&token=<?php echo md5(rand(0, 100000000)); ?>" method="post">
        <h1>车辆资料</h1>
        <span class="h2"></span>
        <li>
            <label>车牌号：</label>
            <select id="card">
            <option></option>
            <?php
	            $s = array(
	            	'table' => 'car'
	            );
	            $r = $mysql->select($s);
	            //print_r($r);
	            foreach($r as $k => $v) {
		            $value = $v['car'];
		            
		            $js = array(
		            	'table' => '`inOut`',
		            	'condition' => "s_card = " . $value['id'],
		            	'order' => 'id desc'
		            );
		            $jr = $mysql->row($js);
		            //print_r($jr);
		            if($jr['fanhuan'] == 1 or !is_array($jr)) {
	            
            ?>
	            <option value='<?php echo json_encode($value); ?>'><?php echo $value['s_card']; ?></option>
	        <?php }} ?>
            </select>
            <input type="hidden" name="s_card" id="s_card" />
        </li>
        <li>
            <label>车辆品牌：</label>
            <div class="show" id="pinpai"></div>
        </li>
        <li>
            <label>车辆型号：</label>
            <div class="show" id="xinghao"></div>
        </li>
        <li>
            <label>车主姓名：</label>
            <div class="show" id="chezhu"></div>
        </li>
        <li>
            <label>车辆颜色：</label>
            <div class="show" id="color"></div>
        </li>

        <li>
            <label>已行驶里程：</label>
            <input name="s_licheng" id="yixingshi" value="" />
        </li>

        
        <h1>客户资料</h1>
        <span class="h2"></span>
        
        <li>
            <label>用户：</label>
            <select id="user">
                <option></option>
                <?php
                $s = array(
                    'table' => 'user'
                );
                $r = $mysql->select($s);
                //print_r($r);
                foreach($r as $k => $v) {
                    $value = $v['user'];
                    //$value['s_cishu'] = 0;
                    $us = array(
                    	'table' => '`inOut`',
                    	'condition' => 's_user = ' . $value['id']
                    );
                    $ur = $mysql->select($us);
                    print_r($ur);
                    if(is_array($ur)){
	                    $uc = count($ur);
                    }else{
	                    $uc = 0;
                    }
                    if($uc != 0) {
	                    $usb = array(
	                    	'table' => '`inOut`',
	                    	'condition' => 's_user = ' . $value['id'] . ' and s_hei = 1'
	                    );
	                    $urb = $mysql->select($usb);
	                    if(is_array($urb)){
		                    $ucb = count($urb);
	                    }else{
		                    $ucb = 0;
	                    }
	                    
	                    $value['s_cishu'] = $uc . "({$ucb}次被加黑处理)";
                    }else{
	                    $value['s_cishu'] = '还未在此租车！';
                    }

                    ?>
                    <option value='<?php echo json_encode($value); ?>'><?php echo $value['s_name'] . '(' . $value['s_phone'] . ')'; ?></option>
                <?php } ?>
            </select>
            <input type="hidden" name="s_user" id="s_name" />
        </li>
        
        <li>
            <label>身份证：</label>
            <div class="show" id="u_card"></div>
        </li>
        <li>
            <label>联系电话：</label>
            <div class="show" id="u_phone"></div>
        </li>
        <li>
            <label>详细地址：</label>
            <div class="show" id="u_address"></div>
        </li>
        <li>
            <label>租车次数：</label>
            <div class="show" id="u_cishu"></div>
        </li>

        <h1>出租信息</h1>
        <span class="h2"></span>
        
        <li>
            <label>租用天数：</label>
            <select id="day" onchange="$('#shijian').data('days', parseInt($('#day').val()) + 1);">
            	<?php
            	$i=1;
            	while($i<=30){
            	?>
            	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            	<?php
            	$i++;
            	}
            	?>
            </select>
        </li>
        
        <li>
            <label>时间选择：</label>
            <div class="show" id="shijian" style="height: 240px; border: 1px #CCC solid;"></div>
        </li>
        
        <li>
            <label>开始时间：</label>
            <div class="show" id="s_start"></div>
            <input type="hidden" name="s_start" id="s_start_v" value="" />
        </li>
        
        <li>
            <label>还车时间：</label>
            <div class="show" id="s_over"></div>
            <input type="hidden" name="s_over" id="s_over_v" value="" />
        </li>
        
        <li>
            <label>押金：</label>
            <div class="show" id="yajin">0</div>
            <input type="hidden" id="s_yajin" name="s_yajin" value="0" />
        </li>

        <li>
            <label>价格：</label>
            <div class="show" id="money">0</div>
        </li>

        <li>
            <label>总价格：</label>
            <input type="text" id="zmoney" name="money" value="0" />
        </li>

        <li>
            <label>实收金额：</label>
            <input name="s_shishou" value="0" />
        </li>
        <li>
            <label>付款方式：</label>
            <select name="s_fangshi">
                <option value="现金">现金</option>
                <option value="信用卡">信用卡</option>
                <option value="普通卡">普通卡</option>
            </select>
        </li>
        <li>
            <label>租车方式：</label>
            <select name="s_zuchefangshi">
            	<option value="门店">门店</option>
                <option value="预定">预定</option>
            </select>
        </li>
        <li>
            <label>经办人：</label>
            <input name="s_jinbanren" value="" />
        </li>

        <li class="textarea">
            <label>备注：</label>
            <textarea name="s_beizhu"></textarea>
        </li>
        
        
        <li class="bu">
            <button>保存</button>
            <span class="result"></span>
        </li>
    </form>
    
    
    	<h1>参数</h1>
        <span class="h2"></span>
        <li>
            <label>年审时间：</label>
            <div class="show" id="nianshen"></div>
        </li>
        <li>
            <label>交强险到期时间：</label>
            <div class="show" id="jiaoqiangxian"></div>
        </li>
        <li>
            <label>商业险到期时间：</label>
            <div class="show" id="shangyexian"></div>
        </li>
        
        <li>
            <label>车辆图片：</label>
            <div class="show" id="photos"></div>
        </li>
</ol>