<?php
/**
 * Created by PhpStorm.
 * User: ireoo
 * Date: 14-1-22
 * Time: 下午8:12
 */

?>

<?php

$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
        $r1 = $mysql->update('car', $_POST, "id = $id");
        //print_r($_POST);
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /?s=car&i=index");
    }
}

?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol table{border-collapse: collapse; width: 100%; margin-bottom: 10px;}
    div.mian ol table tr{margin-bottom: 10px;}
    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}
    
    div.foot{height: 32px; text-align: center;}
    div.foot a{display: inline-block; padding: 0 10px; line-height: 30px; color: #FFF; border: 1px #333 solid;}
    div.foot a.on{background: #FFF; color: #333;}
</style>

<script src="/app/layer/layer.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app/layer/skin/layer.ext.css">
<script type="text/javascript">
	$(
		function() {
			$('a.btn').click(
				function() {
					id = $(this).attr('id');
					$.layer({
					    type : 2,
					    title : '详细信息',
					    iframe : {src : '/xiangxi/car.php?id=' + id},
					    area : ['750px' , '466px'],
					    offset : ['100px','100px']
					});
				}
			);
		}
	);
</script>

<ol>
    <h1>车辆管理</h1>

    <table>
        <thead>
        <tr>
            <td>车牌号</td>
            <td>车辆品牌</td>
            <td>车辆型号</td>
            <td>车主姓名</td>
            <td>车辆颜色</td>
            <td>年审时间</td>
            <td>交强险到期时间</td>
            <td>商业险到期时间</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php
        
        $now = 0;
        $show = 20;
        
        if(isset($_GET['y'])) {
	        $now = $_GET['y'];
        }
        
        $start = $now*$show;
        
        $sql = array(
            'table' => 'car'
        );
        $zong = $mysql->select($sql);
        
        $list = $sql;
        $list['limit'] = "LIMIT $start, $show";

		$re = $mysql->select($list);
        //print_r($s);
        foreach($re as $key => $value) {
            $v = $value['car'];
            ?>
            <tr>
                <td><?php echo $v['s_card']; ?></td>
	            <td><?php echo $v['s_pinpai']; ?></td>
	            <td><?php echo $v['s_xinghao']; ?></td>
	            <td><?php echo $v['s_chezhu']; ?></td>
	            <td><?php echo $v['s_color']; ?></td>
	            <td><?php echo $v['s_nianshen']; ?></td>
	            <td><?php echo $v['s_jiaoqiangxian']; ?></td>
	            <td><?php echo $v['s_shangyexian']; ?></td>
	            <td><a class="btn" id="<?php echo $v['id']; ?>">详细</a></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    
    <div class="foot">
    	<a href="?s=car">首页</a>
    	<?php for($i=0; $i<ceil(count($zong)/$show); $i++) { ?>
    	<a<?php if($now == $i) {echo ' class="on"';} ?> href="?s=car&y=<?php echo $i; ?>"><?php echo $i+1; ?></a>
    	<?php } ?>
    	<a href="?s=car&y=<?php echo floor(count($zong)/$show); ?>">尾页</a>
    </div>

</ol>