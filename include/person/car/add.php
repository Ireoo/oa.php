<?php
$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
        if($mysql->insert('car', $_POST)) {
            echo '<script type="text/javascript">alert("车辆信息已经保存！");</script>';
        }else{
            echo mysql_error();
            print_r($_POST);
        }
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /?s=car&i=add");
    }
}
?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    div.mian ol li a{font-size: 14px; cursor: pointer;}
    div.mian ol li a:hover{text-decoration: underline;}

    div.mian ol li label{display: inline-block; width: 100px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    div.mian ol li input{padding: 5px; font-size: 12px; width: 272px;}
    div.mian ol li input.mini{padding: 5px; font-size: 12px; width: 42px;}
    div.mian ol li input.check, div.mian ol li label.auto{width: auto;}
    div.mian ol li select.max{width: 212px;}
    div.mian ol li select{padding: 5px;}

    div.mian ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    div.mian ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    div.mian ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    div.mian ol li ul li.hover{background: #4898F8; color: #FFF;}
    div.mian ol li ul li.hover ul{display: inline-block;}
    div.mian ol li ul li ul li{display: block; font-size: 12px;}
    div.mian ol li ul li ul li a{font-size: 12px;}
    div.mian ol li ul li ul li a:hover, div.mian ol li ul li ul li:hover a, div.mian ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    div.mian ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script type="text/javascript" src="/app/date/jquery.js"></script>
<script type="text/javascript" src="/app/date/DatePicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	
		$('input.date').each(function() {
		
			var date = $(this);
			
			date.DatePicker({
				format:'Y-m-d',
				date: date.val(),
				current: date.val(),
				starts: 1,
				position: 'r',
				onBeforeShow: function(){
					date.DatePickerSetDate(date.val(), true);
				},
				onChange: function(formated, dates){
					date.val(formated);
					date.DatePickerHide();
				}
			});
			
		});
	
/*
		$('#zheceriqi').DatePicker({
			format:'Y-m-d',
			date: $('#zheceriqi').val(),
			current: $('#zheceriqi').val(),
			starts: 1,
			position: 'r',
			onBeforeShow: function(){
				$('#zheceriqi').DatePickerSetDate($('#zheceriqi').val(), true);
			},
			onChange: function(formated, dates){
				$('#zheceriqi').val(formated);
				$('#zheceriqi').DatePickerHide();
			}
		});
*/
	});
</script>
<link rel="stylesheet" href="/app/date/datepicker.css" type="text/css" />

<script type="text/javascript" src="/include/js/photobox.js"></script>
<script type="text/javascript" src="http://open.web.meitu.com/sources/xiuxiu.js"></script>
<script type="text/javascript">
$(function() {
	photobox('#upload', '#photo');
});
</script>

<ol class="account">

    <form action="?s=car&i=add&token=<?php echo md5(rand(0, 100000000)); ?>" method="post">
        <h1>基本资料</h1>
        <span class="h2"></span>
        <li>
            <label>车牌号：</label>
            <input type="text" name="s_card" value="" />
        </li>

        <li>
            <label>一级分类：</label>
            <select name="s_yiji">
                <option value="自备车">自备车</option>
                <option value="外挂车">挂靠车</option>
            </select>
        </li>

        <li>
            <label>二级分类：</label>
            <select name="s_erji">
                <option value="自备车">自用车</option>
                <option value="外挂车">外挂车</option>
            </select>
        </li>

        <li>
            <label>车辆品牌：</label>
            <input type="text" name="s_pinpai" value="" />
        </li>
        <li>
            <label>车辆型号：</label>
            <input type="text" name="s_xinghao" value="" />
        </li>
        <li>
            <label>车主姓名：</label>
            <input type="text" name="s_chezhu" value="" />
        </li>
        
        <li>
            <label>座位数：</label>
            <input type="text" name="s_zuowei" value="" />
        </li>
        <li>
            <label>尺寸：</label>
            <input class="mini" type="text" name="s_chang" value="" />（长）＊<input class="mini" type="text" name="s_kuan" value="" />（宽）＊<input class="mini" type="text" name="s_gao" value="" />（高）
        </li>
        <li>
            <label>车重：</label>
            <input type="text" name="s_chezhong" value="" />T
        </li>
        
        <li>
            <label>车辆颜色：</label>
            <input type="text" name="s_color" value="" />
        </li>
        <li>
            <label>注册日期：</label>
            <input class="date" type="text" name="s_zhuceriqi" value="<?php echo date('Y-m-d'); ?>" />
        </li>
        <li>
            <label>登记证书编号：</label>
            <input type="text" name="s_dengjizhengshu" value="" />
        </li>
        <li>
            <label>车架号：</label>
            <input type="text" name="s_chejia" value="" />
        </li>

        <li>
            <label>发动机号：</label>
            <input type="text" name="s_fadongji" value="" />
        </li>

        <li>
            <label>已行驶里程：</label>
            <input type="text" name="s_yixingshi" value="" />KM
        </li>
        <li>
            <label>保养程：</label>
            <input type="text" name="s_baoyang" value="" />KM
        </li>
        <li>
            <label>年审时间(按⽉)：</label>
            <input class="date" type="text" name="s_nianshen" value="<?php echo date('Y-m-d'); ?>" />
        </li>
        <li>
            <label>交强险到期时间(按⽇)：</label>
            <input class="date" type="text" name="s_jiaoqiangxian" value="<?php echo date('Y-m-d'); ?>" />
        </li>
        <li>
            <label>商业险到期时间(按⽇)：</label>
            <input class="date" type="text" name="s_shangyexian" value="<?php echo date('Y-m-d'); ?>" />
        </li>
        
        <li>
            <label>租金(按日)：</label>
            <input type="text" name="s_zujin" value="" />元
        </li>
        <li>
            <label>押金：</label>
            <input type="text" name="s_yajin" value="" />元
        </li>
        
        <li>
            <label>车辆图片：</label>
            <input type="text" readonly id="photo" name="s_photos" value="" /><div id="upload" style="display: inline-block; padding-left: 20px;"></div>
        </li>

        <li class="textarea">
            <label>备注：</label>
            <textarea name="s_desc"></textarea>
        </li>
        
        

        

        <li class="bu">
            <button>保存</button>
            <span class="result"></span>
        </li>
    </form>
</ol>