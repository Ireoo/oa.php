<?php
/**
 * Created by PhpStorm.
 * User: Ultra
 * Date: 14-5-10
 * Time: 下午4:28
 */
?>

<?php

if(isset($_GET['del']) and isset($_GET['id'])) {
	$id = $_GET['id'];
	if($mysql->delete('money', 'id = ' . $id)) {
		header("Location: /?s=money&i=zhichu");
	}
}

?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol table{border-collapse: collapse; width: 100%; margin-bottom: 10px;}
    div.mian ol table tr{margin-bottom: 10px;}

    div.mian ol table tr.red{background: red;}
    div.mian ol table tr.red td{color: #FFF;}

    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}
    
    div.foot{height: 32px; text-align: center;}
    div.foot a{display: inline-block; padding: 0 10px; line-height: 30px; color: #FFF; border: 1px #333 solid;}
    div.foot a.on{background: #FFF; color: #333;}
</style>

<ol>
    <h1>支出记录</h1>

    <table>
        <thead>
        <tr>
            <td>车牌</td>
            <td>费用说明</td>
            <td>金额</td>
            <td>交易时间</td>
            <td>备注</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <?php
        
        $now = 0;
        $show = 20;
        
        if(isset($_GET['y'])) {
	        $now = $_GET['y'];
        }
        
        $start = $now*$show;
        
        $sql = array(
            'table' => 'money',
            'condition' => 's_fangshi = "out"'
        );
        $zong = $mysql->select($sql);
        
        $list = $sql;
        $list['limit'] = "LIMIT $start, $show";
        //print_r($list);
        $re = $mysql->select($list);
        foreach($re as $key => $value) {
            $v = $value['money'];
            ?>
            <tr>

                <?php

                $id = $v['s_card'];
                $s = array(
                    'table' => 'car',
                    'condition' => 'id = ' . $id
                );
                $r = $mysql->row($s);
                //print_r($s);

                ?>

                <td><?php echo $r['s_card']; ?></td>
                <td><?php echo $v['s_shuoming']; ?></td>

                <td><?php echo $v['money']; ?></td>
                <td><?php echo date('Y-m-d', $v['timer']); ?></td>
                <td><?php echo $v['s_desc']; ?></td>
				<td>
	                <a class="btn" id="<?php echo $v['id']; ?>">修改</a>
	                <a href="?s=money&i=zhichu&id=<?php echo $v['id']; ?>&del=yes">删除</a>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    
    <div class="foot">
    	<a href="?s=money&i=zhichu">首页</a>
    	<?php for($i=0; $i<ceil(count($zong)/$show); $i++) { ?>
    	<a<?php if($now == $i) {echo ' class="on"';} ?> href="?s=money&i=zhichu&y=<?php echo $i; ?>"><?php echo $i+1; ?></a>
    	<?php } ?>
    	<a href="?s=money&i=zhichu&y=<?php echo floor(count($zong)/$show); ?>">尾页</a>
    </div>

</ol>
<script src="/app/layer/layer.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app/layer/skin/layer.ext.css">
<script type="text/javascript">
    $(
        function() {
            
            $('a.btn').click(
                function() {
                	id = $(this).attr('id');
                    $.layer({
                        type : 2,
                        title : '修改信息',
                        iframe : {src : 'xiangxi/money.php?id=' + id},
                        area : ['750px' , '466px'],
                        offset : ['100px','100px']
                    });
                }
            );
        }
    );
</script>