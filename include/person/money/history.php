<?php
/**
 * Created by PhpStorm.
 * User: ireoo
 * Date: 14-1-22
 * Time: 下午8:12
 */

?>

<?php



?>
<style type="text/css">
    div.mian ol{padding-bottom: 100px;}
    div.mian ol table{border-collapse: collapse; width: 100%;}
    div.mian ol table tr{margin-bottom: 10px;}
    
    div.mian ol table tr.red{background: red;}
    div.mian ol table tr.red td{color: #FFF;}
    
    div.mian ol table tr td{border: 1px #CCC solid; padding: 5px; font-size: 12px;}
    div.mian ol table thead tr td{background: #4898F8; color: #FFF; border-color: #4898F8;}
    div.mian ol table tr td.first{background: none; border: none; width: 40px; padding: 0;}
    div.mian ol table tr td.foot{background: none; border: none; width: 215px;}
    div.mian ol table tbody tr td a{padding: 3px; background: #4898F8; color: #FFF; cursor: pointer;}
    div.mian ol table tbody tr td img.headimg{width: 30px; height: 30px;}

    div.mian ol table tbody tr td span.red{color: red;}

    button{padding: 5px 20px;}
</style>

<ol>
    <h1>历史记录</h1>

    <table>
        <thead>
        <tr>
            <td>日期</td>
            <td>收入</td>
            <td>支出</td>
            <td>盈利</td>
        </tr>
        </thead>
        <tbody>
        <?php
        $sql = array(
            'table' => 'money'
        );
        $re = $mysql->select($sql);
        //print_r($s);
        $start = strtotime(date('Y/m/01', $re[0]['money']['timer']));
        $zhichu = 0;
        $shouru = 0;
        //print_r(date('Y/m', $start));
        foreach($re as $key => $value) {
            $v = $value['money'];
	        
            $over = strtotime(date('Y', $start) . '/' . (date('m', $start) + 1) . '/01');
	        //print_r($over);
	        $timer = $v['timer'];
	        //print_r(date('Y/m/d', $timer));
	        $fangshi = $v['s_fangshi'];
	        //print_r($v['money']);
	        if($timer > $over) {
	        	$shijian = date('Y/m', $start);
	        	$ying = $shouru - $zhichu;
	            echo "<tr><td>{$shijian}</td><td>{$shouru}</td><td>{$zhichu}</td><td>{$ying}</td></tr>";
	            if($fangshi == 'out') {
			        $zhichu = $v['money'];
		        }else{
			        $shouru = $v['money'];
		        }
	            $start = strtotime(date('Y/m/01', $timer));
	            //echo '[1]';
	        }else{
		        if($fangshi == 'in') {
			        $shouru += $v['money'];
			        //echo '[2]';
		        }else{
			        $zhichu += $v['money'];
			        //echo '[3]';
		        }
		        
	        }
        }
        $shijian = date('Y/m', $start);
    	$ying = $shouru - $zhichu;
        echo "<tr><td>{$shijian}</td><td>{$shouru}</td><td>{$zhichu}</td><td>{$ying}</td></tr>";
        ?>
        </tbody>
    </table>

</ol>