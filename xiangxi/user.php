<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set("PRC");
session_start();
$get_start_time = time();
require_once("../lib/mysql.class.php");
require_once("../include/php/php.php");
//if(!isset($_SESSION['user'])) header("Location: http://{$_SERVER['HTTP_HOST']}/login?url=" . curPageURL());
$mysql = new mysql;

$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
        if($mysql->update('user', $_POST, 'id = ' . $_GET['id'])) {
            echo '<script type="text/javascript">alert("会员信息已经修改！");</script>';
        }else{
            echo mysql_error();
            print_r($_POST);
        }
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /xiangxi/user.php?id=" . $_GET['id']);
    }
}

$s = array(
	'table' => 'user',
	'condition' => 'id = ' . $_GET['id']
);
$user = $mysql->row($s);
//print_r($user);
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../include/css/head.css" rel="stylesheet" type="text/css">
    <link href="../include/css/i.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../include/js/jquery.js"></script>
    <style type="text/css">
        div.top div.logo{float: left;}
        div.top div.logo a{padding: 5px; font-size: 20px; font-weight: bold; background: #4898F8; color: #FFF; border-radius: 5px;}
        div.top div.logo input.keyword{padding: 5px; font-size: 20px; width: 500px;}
    </style>
</head>
<body>
<style type="text/css">
    ol{padding: 10px;}
    ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    ol li a{font-size: 14px; cursor: pointer;}
    ol li a:hover{text-decoration: underline;}

    ol li label{display: inline-block; width: 100px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    ol li input{padding: 5px; font-size: 12px; width: 272px;}
    ol li input.check, ol li label.auto{width: auto;}
    ol li select.max{width: 212px;}
    ol li select{padding: 5px;}

    ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    ol li ul li.hover{background: #4898F8; color: #FFF;}
    ol li ul li.hover ul{display: inline-block;}
    ol li ul li ul li{display: block; font-size: 12px;}
    ol li ul li ul li a{font-size: 12px;}
    ol li ul li ul li a:hover, ol li ul li ul li:hover a, ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script type="text/javascript" src="/include/js/photobox.js"></script>
<script type="text/javascript" src="http://open.web.meitu.com/sources/xiuxiu.js"></script>
<script type="text/javascript">
$(function() {
	photobox('#upload', '#cardPhoto');
	photobox('#upload_d', '#cardPhoto_d');
});
</script>

<ol class="account">

    <form action="/xiangxi/user.php?id=<?php echo $_GET['id']; ?>&token=<?php echo md5(rand(0, 100000000)); ?>" method="post">
        <h1>基本资料</h1>
        <span class="h2"></span>
        <li>
            <label>姓名：</label>
            <input type="text" name="s_name" value="<?php echo $user['s_name']; ?>" />
        </li>
        <li>
            <label>性别：</label>
            <select name="s_sex">
            	<option<?php if($user['sex'] == '男') {echo " selected";} ?> value="男">男</option>
            	<option<?php if($user['sex'] == '女') {echo " selected";} ?> value="女">女</option>
            </select>
        </li>
        <li>
            <label>身份证：</label>
            <input type="text" name="s_card" value="<?php echo $user['s_card']; ?>" />
        </li>
        <li>
            <label>身份证照片：</label>
            <input type="text" readonly id="cardPhoto" name="s_cardPhoto" value="<?php echo $user['s_cardPhoto']; ?>" /><div id="upload" style="display: inline-block; padding-left: 20px;"></div>
        </li>
        <li>
            <label>联系电话：</label>
            <input type="text" name="s_phone" value="<?php echo $user['s_phone']; ?>" />
        </li>

        <li>
            <label>现居地址：</label>
            <input type="text" name="s_address" value="<?php echo $user['s_address']; ?>" />
        </li>

        <li class="textarea">
            <label>备注：</label>
            <textarea name="s_beizhu"><?php echo $user['s_beizhu']; ?></textarea>
        </li>
        
        
        <h1>担保人</h1>
        <span class="h2"></span>
        <li>
            <label>担保人姓名：</label>
            <input type="text" name="s_name_d" value="<?php echo $user['s_name_d']; ?>" />
        </li>
        <li>
            <label>担保人性别：</label>
            <select name="s_sex_d">
            	<option<?php if($user['sex_d'] == '男') {echo " selected";} ?> value="男">男</option>
            	<option<?php if($user['sex_d'] == '女') {echo " selected";} ?> value="女">女</option>
            </select>
        </li>
        <li>
            <label>担保人电话：</label>
            <input type="text" name="s_phone_d" value="<?php echo $user['s_phone_d']; ?>" />
        </li>
        
        <li>
            <label>担保人身份证：</label>
            <input type="text" name="s_card_d" value="<?php echo $user['s_card_d']; ?>" />
        </li>
        <li>
            <label>担保人身份证照片：</label>
            <input type="text" readonly id="cardPhoto_d" name="s_cardphoto_d" value="<?php echo $user['s_cardphoto_d']; ?>" /><div id="upload_d" style="display: inline-block; padding-left: 20px;"></div>
        </li>
        
        <li class="bu">
            <button>保存</button>
            <span class="result"></span>
        </li>
    </form>
</ol>
</body>
</html>