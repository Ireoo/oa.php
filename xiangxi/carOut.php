<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set("PRC");
session_start();
$get_start_time = time();
require_once("../lib/mysql.class.php");
require_once("../include/php/php.php");
//if(!isset($_SESSION['user'])) header("Location: http://{$_SERVER['HTTP_HOST']}/login?url=" . curPageURL());
$mysql = new mysql;

$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
    
    	
    	$wucha = $_POST['wucha'];
    	$card = $_POST['s_card'];
    	unset($_POST['wucha']);
    	unset($_POST['s_card']);
        $r1 = $mysql->update('`inOut`', $_POST, "id = {$_GET['id']}");
        if(isset($wucha)) {
	        $s = array(
	        	's_card' => $card,
	        	's_fangshi' => 'out',
	        	's_shuoming' => '车辆退还时返还误差，总价 ' . $_POST['money'] . ' 元，实收 ' . $_POST['s_shishou'] . ' 元。',
	        	'money' => $wucha,
	        	'timer' => time()
	        );
	        $mysql->insert('money', $s);
        }
        //print_r($_POST);
        
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /xiangxi/carOut.php?id=" . $_GET['id']);
    }
}

$id = $_GET['id'];
$s = array(
	'table' => '`inOut`',
	'condition' => 'id = ' . $id
);
$show = $mysql->row($s);
//print_r($show);
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../include/css/head.css" rel="stylesheet" type="text/css">
    <link href="../include/css/i.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../include/js/jquery.js"></script>
    <style type="text/css">
        div.top div.logo{float: left;}
        div.top div.logo a{padding: 5px; font-size: 20px; font-weight: bold; background: #4898F8; color: #FFF; border-radius: 5px;}
        div.top div.logo input.keyword{padding: 5px; font-size: 20px; width: 500px;}
    </style>
</head>
<body>
<style type="text/css">
    ol{padding-bottom: 100px; padding: 10px;}
    ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    ol li a{font-size: 14px; cursor: pointer;}
    ol li a:hover{text-decoration: underline;}

    ol li label{display: inline-block; width: 150px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    ol li input{padding: 5px; font-size: 12px; width: 272px;}
    ol li input.check, div.mian ol li label.auto{width: auto;}
    ol li select.max{width: 212px;}
    ol li select{padding: 5px;}
    
    ol li div.show{display: inline-block; padding: 5px;}

    ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    ol li ul li.hover{background: #4898F8; color: #FFF;}
    ol li ul li.hover ul{display: inline-block;}
    ol li ul li ul li{display: block; font-size: 12px;}
    ol li ul li ul li a{font-size: 12px;}
    ol li ul li ul li a:hover, div.mian ol li ul li ul li:hover a, div.mian ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script language="JavaScript" type="text/javascript" src="../app/jCal/jquery.animate.clip.js"></script>
<script language="JavaScript" type="text/javascript" src="../app/jCal/jCal.min.js"></script>

<link rel="stylesheet" type="text/css" href="../app/jCal/jCal.css">

	<script>
/*
		$(document).ready(function () {
			
				

					$('#shijian').jCal({
					day:			new Date(),
					days:			2,
					showMonths:		2,
					monthSelect:	true,
					sDate:			new Date(),
	
					callback:		function (day, days) {
							$('#s_start_v').val(day.getTime()/1000);
							$('#s_start').text(day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate());
							var lin = new Date(day.setDate( day.getDate() + days - 1 ));
							$('#s_over_v').val(lin.getTime()/1000);
							$('#s_over').text(lin.getFullYear() + '-' + (lin.getMonth() + 1) + '-' + lin.getDate());
							return true;
						}
					});


            $('#card').change(function() {
                var json = eval('(' + $(this).val() + ')');
                $('#pinpai').text(json.s_pinpai);
                $('#s_card').val(json.id);
                $('#xinghao').text(json.s_xinghao);
                $('#chezhu').text(json.s_chezhu);
                $('#color').text(json.s_color);
                $('#yixingshi').text(json.s_yixingshi);

                $('#nianshen').text(json.s_nianshen);
                $('#jiaoqiangxian').text(json.s_jiaoqiangxian);
                $('#shangyexian').text(json.s_shangyexian);
                $('#photos').text(json.s_photo);
            });

            $('#user').change(function() {
                var json = eval('(' + $(this).val() + ')');
                $('#s_name').val(json.id);
                //alert(json.s_card);
                $('#u_card').text(json.s_card);
                $('#u_phone').text(json.s_phone);
                $('#u_address').text(json.s_address);
                $('#u_cishu').text(json.s_cishu);
            });
				
				$('#money').keyup(function() {
					var t = new Date();
					t.setTime($('#s_start_v').val()*1000);
					//alert($('#s_start').text());
					//alert(t.getDate());
					var c = $('#day').val();
					$('#zmoney').val(0);
					t.setDate(t.getDate() - 1);
					for (var i=0; i<c; i++) {
						t.setDate(t.getDate() + 1);
						//alert(t.getDay());
						switch(t.getDay()) {
							case 0:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1.1));
								break;
							case 1:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1));
								break;
							case 2:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1));
								break;
							case 3:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1));
								break;
							case 4:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1));
								break;
							case 5:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1));
								break;
							case 6:
								$('#zmoney').val(parseInt($('#zmoney').val()*1 + $(this).val()*1.1));
								break;
						}
						
					}
					$('#zmoney').val(parseInt($('#zmoney').val()) + parseInt($('#yajin').val()));
				});
				
		});
*/
		$(
			function() {
				
				$('#weizhang').keyup(function() {
					//alert('1');
					$('#fanhuan').val(parseInt($('#weizhangyajin').text()) - parseInt($('#weizhang').val()));
				});
				
			}
		);
	</script>


<ol class="account">

    
        <h1>车辆资料</h1>
        <span class="h2"></span>
        <?php
	            $s = array(
	            	'table' => 'car',
	            	'condition' => 'id = ' . $show['s_card']
	            );
	            $r = $mysql->row($s);
            ?>
        <li>
            <label>车牌号：</label>
            <div class="show" id="card"><?php echo $r['s_card']; ?></div>
        </li>
        <li>
            <label>车辆品牌：</label>
            <div class="show" id="pinpai"><?php echo $r['s_pinpai']; ?></div>
        </li>
        <li>
            <label>车辆型号：</label>
            <div class="show" id="xinghao"><?php echo $r['s_xinghao']; ?></div>
        </li>
        <li>
            <label>车主姓名：</label>
            <div class="show" id="chezhu"><?php echo $r['s_chezhu']; ?></div>
        </li>
        <li>
            <label>车辆颜色：</label>
            <div class="show" id="color"><?php echo $r['s_color']; ?></div>
        </li>

        <li>
            <label>已行驶里程：</label>
            <div class="show" id="color"><?php echo $show['s_licheng']; ?></div>
        </li>

        
        <h1>客户资料</h1>
        <span class="h2"></span>
        
        <?php
	            $s = array(
	            	'table' => 'user',
	            	'condition' => 'id = ' . $show['s_user']
	            );
	            $r = $mysql->row($s);
            ?>
        
        <li>
            <label>用户：</label>
            <input type="text" name="s_card" value="" />
            <div class="show" id="u_name"><?php echo $r['s_name']; ?></div>
        </li>
        
        <li>
            <label>身份证：</label>
            <div class="show" id="u_card"><?php echo $r['s_card']; ?></div>
        </li>
        <li>
            <label>联系电话：</label>
            <div class="show" id="u_phone"><?php echo $r['s_phone']; ?></div>
        </li>
        <li>
            <label>详细地址：</label>
            <div class="show" id="u_address"><?php echo $r['s_address']; ?></div>
        </li>

        <h1>出租信息</h1>
        <span class="h2"></span>
        
        <li>
            <label>开始时间：</label>
            <div class="show" id="s_start"><?php echo date('Y-m-d', $show['s_start']); ?></div>
        </li>
        
        <li>
            <label>还车时间：</label>
            <div class="show" id="s_over"><?php echo date('Y-m-d', $show['s_over']); ?></div>
        </li>
        
<!--
        <li>
            <label>押金：</label>
            <div class="show" id="s_over"><?php echo $show['money']; ?></div>
        </li>

        <li>
            <label>价格：</label>
            <div class="show" id="s_over"><?php echo $show['money']; ?></div>
        </li>
-->

        <li>
            <label>总价格：</label>
            <div class="show" id="s_over"><?php echo $show['money']; ?></div>
        </li>

        <li>
            <label>实收金额：</label>
            <div class="show" id="s_over"><?php echo $show['s_shishou']; ?></div>
        </li>
        <li>
            <label>付款方式：</label>
            <div class="show" id="s_over"><?php echo $show['s_fangshi']; ?></div>
        </li>
        <li>
            <label>租车方式：</label>
            <div class="show" id="s_over"><?php echo $show['s_zuchefangshi']; ?></div>
        </li>
        
        <li>
            <label>经办人：</label>
            <div class="show" id="s_jinbanren"><?php echo $show['s_jinbanren']; ?></div>
        </li>
        
        
<?php
	//print_r($show['fanhuan']);
	if($show['fanhuan'] != 1) {	
?>
    
    <form action="/xiangxi/carOut.php?id=<?php echo $_GET['id']; ?>&token=<?php echo md5(rand(0, 100000000)); ?>" method="post">
    	<h1>参数</h1>
        <span class="h2"></span>
        <li>
            <label>还车里程：</label>
            <input type="text" name="s_licheng_over" value="" />
        </li>
        <li>
            <label>车损情况：</label>
            <a href="#" onclick="return false;">上传照片</a>
        </li>

        <input type="hidden" name="fanhuan" value="1" />
        <input type="hidden" name="wucha" value="<?php echo $show['s_shishou'] - $show['money']; ?>" />
        <input type="hidden" name="s_card" value="<?php echo $show['s_card']; ?>" />
        <input type="hidden" name="s_shishou" value="<?php echo $show['s_shishou']; ?>" />
        <input type="hidden" name="money" value="<?php echo $show['money']; ?>" />
        
        <li>
            <label>备注：</label>
            <textarea name="s_beizhu"><?php echo $show['s_beizhu']; ?></textarea>
        </li>
        
        <li>
            <label> </label>
            <button>还车</button>
        </li>
        
    </form>
<?php
	
	}
	
?>
</ol>
</body>
</html>