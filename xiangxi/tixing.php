<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set("PRC");
session_start();
$get_start_time = time();
require_once("../lib/mysql.class.php");
require_once("../include/php/php.php");
//if(!isset($_SESSION['user'])) header("Location: http://{$_SERVER['HTTP_HOST']}/login?url=" . curPageURL());
$mysql = new mysql;

$token = '';
if(isset($_GET['token'])) $token = $_GET['token'];

if($token != '') {
    if($token != $_SESSION['token']) {
    	
    	$timer = $_POST['timer'];
    	$_POST['timer'] = strtotime($timer);
    
        if($mysql->insert('tixing', $_POST)) {
            echo '<script type="text/javascript">alert("提醒信息已经保存！");</script>';
        }else{
            echo mysql_error();
            print_r($_POST);
        }
        $_SESSION['token'] = $_GET['token'];
    }else{
        header("Location: /xiangxi/tixing.php");
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../include/css/head.css" rel="stylesheet" type="text/css">
    <link href="../include/css/i.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../include/js/jquery.js"></script>
    <style type="text/css">
        div.top div.logo{float: left;}
        div.top div.logo a{padding: 5px; font-size: 20px; font-weight: bold; background: #4898F8; color: #FFF; border-radius: 5px;}
        div.top div.logo input.keyword{padding: 5px; font-size: 20px; width: 500px;}
    </style>
</head>
<body>
<style type="text/css">
    ol{padding: 10px;}
    ol li{font-size: 12px; padding-top: 5px; padding-bottom: 10px;}
    ol li a{font-size: 14px; cursor: pointer;}
    ol li a:hover{text-decoration: underline;}

    ol li label{display: inline-block; width: 100px; font-size: 12px; font-weight: bold; color: #000; padding: 5px; vertical-align: top;}
    ol li input{padding: 5px; font-size: 12px; width: 272px;}
    ol li input.check, ol li label.auto{width: auto;}
    ol li select.max{width: 212px;}
    ol li select{padding: 5px;}

    ol li ul.city{display: inline-block; width: 680px; margin-left: -4px; border: 1px solid #ffd88a; background: #FFE69F; padding: 10px;}
    ol li ul li{position: relative; display: inline-block; font-size: 14px; padding: 5px; cursor: pointer;}
    ol li ul li ul{position: absolute; display: none; left: 36px; top: 0; z-index: 3; width: 40px; border: 1px solid #4898F8; background: #FFF;}
    ol li ul li.hover{background: #4898F8; color: #FFF;}
    ol li ul li.hover ul{display: inline-block;}
    ol li ul li ul li{display: block; font-size: 12px;}
    ol li ul li ul li a{font-size: 12px;}
    ol li ul li ul li a:hover, ol li ul li ul li:hover a, ol li ul li ul li:hover{background: #4898F8; color: #FFF; text-decoration: none;}
    ol li ul h1{display: inline-block; border: none; font-size: 12px; font-weight: normal; color: #333; background: RGB(201, 201, 201); cursor: pointer;}

    textarea{width: 272px; padding: 5px; height: 80px;}
    button{padding: 5px 20px;}
</style>

<script type="text/javascript" src="/app/date/jquery.js"></script>
<script type="text/javascript" src="/app/date/DatePicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	
		$('input.date').each(function() {
		
			var date = $(this);
			
			date.DatePicker({
				format:'Y-m-d',
				date: date.val(),
				current: date.val(),
				starts: 1,
				position: 'r',
				onBeforeShow: function(){
					date.DatePickerSetDate(date.val(), true);
				},
				onChange: function(formated, dates){
					date.val(formated);
					date.DatePickerHide();
				}
			});
			
		});
	
	});
</script>
<link rel="stylesheet" href="/app/date/datepicker.css" type="text/css" />

<ol class="account">

    <form action="?token=<?php echo md5(rand(0, 100000000)); ?>" method="post">

        <li>
            <label>时间：</label>
            <input class="date" type="text" name="timer" value="<?php echo date('Y-m-d'); ?>" />
        </li>
        <li>
            <label>内容：</label>
            <textarea name="com"></textarea>
        </li>
        
        <li class="bu">
            <button>保存</button>
            <span class="result"></span>
        </li>
    </form>
</ol>
</body>
</html>